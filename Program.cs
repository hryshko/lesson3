﻿namespace Lesson3
{
    class Program
    {
        static void Main(string[] args)
        {
            Task1();
            Task2();
            Task3();
            Task4();
        }

        static void Task1()
        {
            int operand1 = 1;
            int operand2 = 2;

            Console.WriteLine("Input the sign of an arithmetic operation please. You can input only: +, -, *, /.");
            var sign = Console.ReadLine();

            switch (sign)
            {
                case "+":
                    int sum = operand1 + operand2;
                    Console.WriteLine(sum);
                    break;

                case "-":
                    int difference = operand1 - operand2;
                    Console.WriteLine(difference);
                    break;

                case "*":
                    int multiplication = operand1 * operand2;
                    Console.WriteLine(multiplication);
                    break;

                case "/":
                    if (operand2 != 0)
                    {
                        double division = operand1 / operand2;
                        Console.WriteLine(division);
                    }

                    else
                    {
                        Console.WriteLine("Cannot be divided by zero!");
                        return;
                    }
                    break;

                default:
                    Console.WriteLine("Incorrect arithmetic sign!");
                    return;
            }
        }


        static void Task2() {

            Console.WriteLine("Please input any number.");
            int input = int.Parse(Console.ReadLine());

            if (input >= 0 && input <= 14)
            {
                Console.WriteLine("The number falls in the range of 0-14");
            }

                else if (input >= 15 && input <= 35)
            {

                Console.WriteLine("The number falls in the range of 15-35");
            }

                else if (input >= 36 && input <= 49)
            {
                Console.WriteLine("The number falls in the range of 36-49");
            }

                 else if (input >= 50 && input <= 100)
            {
                Console.WriteLine("The number falls in the range of 50-100");
            }

                else
            {
                Console.WriteLine("The number doesn't falls in any ranges.");
            };

        }


        static void Task3() {

            Console.WriteLine("Please input any word about weather in Russian. I'll try to translate it into English.");
            string word = Console.ReadLine();

            switch (word) {

                case "солнце":
                    Console.WriteLine("sun");
                    break;

                case "ветер":
                    Console.WriteLine("wind");
                    break;

                case "дождь":
                    Console.WriteLine("rain");
                    break;

                case "туман":
                    Console.WriteLine("fog");
                    break;

                case "гроза":
                    Console.WriteLine("thunderstorm");
                    break;

                case "облака":
                    Console.WriteLine("clouds");
                    break;

                case "снег":
                    Console.WriteLine("snow");

                    break;

                case "метель":
                    Console.WriteLine("snowstorm");
                    break;

                case "циклон":
                    Console.WriteLine("cyclone");
                    break;

                case "атмосферный фронт":
                    Console.WriteLine("atmospheric front");
                    break;

                default:
                    Console.WriteLine("My data base doesn't have this word...");
                    return;
            }
        }
        static void Task4() {

            Console.WriteLine("Please input any number and I'll say to you is it a paired number or an unpaired number.");
            int number = int.Parse(Console.ReadLine());

            if (number % 2 == 0)  {
                Console.WriteLine("The number is pared.");
             }

            else
            {
                Console.WriteLine("The number is unpared.");
            }
            
        }
    }
}

     
